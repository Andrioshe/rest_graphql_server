"""Mainfile for starting the application and setting the port and uri for rest and graphql"""
from flask import Flask
from flask_graphql import GraphQLView
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin
from flask_potion import Api

from database import resetdb, load_fixtures
from resources import PersonResource, MeetingResource, PersonMeetingResource, CompanyResource, \
    SectorResource, LocationResource, RelationshipResource, RelationTypeResource
from schema import schema


class EnhancedModelView(ModelView):
    can_view_details = True


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile("settings.py")
    app.secret_key = app.config['SECRET_KEY']
    from database import db
    db.init_app(app)
    return app


def main(host, port, debug=True):
    app.add_url_rule(
        '/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))
    admin = Admin(app, name="Thesis", template_mode="bootstrap3", url="/admin")
    from models import Person, Meeting, Relationship, Company, Sector, RelationType, Location, PersonsMeetings
    from database import db
    admin.add_view(EnhancedModelView(Person, db.session))
    admin.add_view(EnhancedModelView(Meeting, db.session))
    admin.add_view(EnhancedModelView(Relationship, db.session))
    admin.add_view(EnhancedModelView(Company, db.session))
    admin.add_view(EnhancedModelView(Sector, db.session))
    admin.add_view(EnhancedModelView(RelationType, db.session))
    admin.add_view(EnhancedModelView(Location, db.session))
    admin.add_view(EnhancedModelView(PersonsMeetings, db.session))
    if __name__ == '__main__':
        app.run(debug=debug, host=host, port=port)


def startapp(host="0.0.0.0", port=5000):
    """Starts the Flask-Todo."""
    main(host, int(port))


app = create_app()
app.app_context().push()

api = Api(app, prefix="/api")
resources = [PersonResource, MeetingResource, PersonMeetingResource, CompanyResource, SectorResource,
             LocationResource, RelationshipResource, RelationTypeResource]
for resource in resources:
    api.add_resource(PersonResource)
    api.add_resource(MeetingResource)
    api.add_resource(CompanyResource)
    api.add_resource(SectorResource)
    api.add_resource(LocationResource)
    api.add_resource(RelationshipResource)
    api.add_resource(RelationTypeResource)
    api.add_resource(PersonMeetingResource)

resetdb()
load_fixtures()
startapp()
