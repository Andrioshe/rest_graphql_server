"""REST specific Ressources are being declared here."""
from flask_potion import ModelResource

from models import Person, Sector, Company, Location, Relationship, RelationType, Meeting, PersonsMeetings


class PersonResource(ModelResource):
    class Meta:
        model = Person


class SectorResource(ModelResource):
    class Meta:
        model = Sector


class CompanyResource(ModelResource):
    class Meta:
        model = Company


class LocationResource(ModelResource):
    class Meta:
        model = Location


class RelationshipResource(ModelResource):
    class Meta:
        model = Relationship


class RelationTypeResource(ModelResource):
    class Meta:
        model = RelationType


class MeetingResource(ModelResource):
    class Meta:
        model = Meeting


class PersonMeetingResource(ModelResource):
    class Meta:
        model = PersonsMeetings
