"""GraphQL Scheme are being declared in this class"""
import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from models import Person as PersonModel
from models import Meeting as MeetingModel
from models import Location as LocationModel
from models import Sector as SectorModel
from models import Relationship as RelationshipModel
from models import RelationType as RelationTypeModel
from models import Company as CompanyModel
from models import PersonsMeetings as PersonsMeetingsModel


class Person(SQLAlchemyObjectType):
    class Meta:
        model = PersonModel
        interfaces = (graphene.relay.Node,)


class Meeting(SQLAlchemyObjectType):
    class Meta:
        model = MeetingModel
        interfaces = (graphene.relay.Node,)


class Location(SQLAlchemyObjectType):
    class Meta:
        model = LocationModel
        interfaces = (graphene.relay.Node,)


class Relationship(SQLAlchemyObjectType):
    class Meta:
        model = RelationshipModel
        interfaces = (graphene.relay.Node,)


class Sector(SQLAlchemyObjectType):
    class Meta:
        model = SectorModel
        interfaces = (graphene.relay.Node,)


class RelationType(SQLAlchemyObjectType):
    class Meta:
        model = RelationTypeModel
        interfaces = (graphene.relay.Node,)


class Company(SQLAlchemyObjectType):
    class Meta:
        model = CompanyModel
        interfaces = (graphene.relay.Node,)


class PersonsMeetings(SQLAlchemyObjectType):
    class Meta:
        model = PersonsMeetingsModel
        interfaces = (graphene.relay.Node,)


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    persons = graphene.List(Person)
    meetings = graphene.List(Meeting)
    locations = graphene.List(Location)
    companies = graphene.List(Company)
    sectors = graphene.List(Sector)
    relationships = graphene.List(Relationship)
    relation_types = graphene.List(RelationType)
    persons_meetings = graphene.List(PersonsMeetings)

    def resolve_persons(self, info):
        query = Person.get_query(info)
        return query.all()

    def resolve_meetings(self, info):
        query = Meeting.get_query(info)
        return query.all()

    def resolve_companies(self, info):
        query = Company.get_query(info)
        return query.all()

    def resolve_locations(self, info):
        query = Location.get_query(info)
        return query.all()

    def resolve_sectors(self, info):
        query = Sector.get_query(info)
        return query.all()

    def resolve_relationships(self, info):
        query = Relationship.get_query(info)
        return query.all()

    def resolve_relation_types(self, info):
        query = RelationType.get_query(info)
        return query.all()

    def resolve_persons_meetings(self, info):
        query = PersonsMeetings.get_query(info)
        return query.all()


schema = graphene.Schema(query=Query, types=[Person, Meeting, Location, Sector, Company, RelationType, Relationship, PersonsMeetings])
