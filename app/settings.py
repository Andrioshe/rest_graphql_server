"""Application configurations are done here"""
import os

DBDIR = os.path.join(os.getcwd(), "db")
DBPATH = os.path.join(os.getcwd(), "db", "database.db")
BACKEND = "sqlite3"

SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(DBPATH)
SQLALCHEMY_ECHO = False
SQLALCHEMY_RECORD_QUERIES = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
POTION_DECORATE_SCHEMA_ENDPOINTS = False
POTION_DEFAULT_PER_PAGE = 2000000
POTION_MAX_PER_PAGE = 2000000

SECRET_KEY = "av3rys3ckretk3y!@#{}!;;"
