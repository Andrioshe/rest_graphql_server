"""For making the application understand how the database is built the database tables are being defined here"""
from datetime import datetime
from database import db


class PersonsMeetings(db.Model):
    __tablename__ = "persons_meetings"
    person_id = db.Column(db.Integer, db.ForeignKey('persons.id'), primary_key=True)
    meeting_id = db.Column(db.Integer, db.ForeignKey('meetings.id'), primary_key=True)
    person = db.relationship("Person", back_populates="meetings")
    meeting = db.relationship("Meeting", back_populates="persons")


class Meeting(db.Model):
    __tablename__ = "meetings"
    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
    description = db.Column(db.String(255), nullable=False)
    location_id = db.Column(db.Integer, db.ForeignKey('locations.id'), nullable=False)
    persons = db.relationship("PersonsMeetings", back_populates="meeting")


class Person(db.Model):
    __tablename__ = "persons"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(35), nullable=False)
    last_name = db.Column(db.String(35), nullable=False)
    gender = db.Column(db.Boolean, nullable=False)


class Location(db.Model):
    __tablename__ = "locations"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(35), nullable=False)
    street = db.Column(db.String(35), nullable=False)
    city = db.Column(db.String(35), nullable=False)
    zip = db.Column(db.String(35), nullable=False)
    country = db.Column(db.String(35), nullable=False)
    persons = db.relationship('Person', backref='location', lazy=True)
    companies = db.relationship('Company', backref='location', lazy=True)
    meetings = db.relationship('Meeting', backref='location', lazy=True)


class Relationship(db.Model):
    __tablename__ = "relationships"
    id = db.Column(db.Integer, primary_key=True)
    person_1_id = db.Column(db.Integer, db.ForeignKey('persons.id'), nullable=False)
    person_2_id = db.Column(db.Integer, db.ForeignKey('persons.id'), nullable=False)
    relation_type = db.Column(db.Integer, db.ForeignKey('relation_types.id'), nullable=False)


class Company(db.Model):
    __tablename__ = "companies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(35), nullable=False)
    sector_id = db.Column(db.Integer, db.ForeignKey('sectors.id'), nullable=False)
    location_id = db.Column(db.Integer, db.ForeignKey('locations.id'), nullable=False)


class Sector(db.Model):
    __tablename__ = "sectors"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(35), nullable=False)
    companies = db.relationship('Company', backref='sectors', lazy=True)


class RelationType(db.Model):
    __tablename__ = "relation_types"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(35), nullable=False)
