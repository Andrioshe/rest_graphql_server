from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def dropdb():
    """Drop database and tables."""
    from app import app
    from sqlalchemy_utils import database_exists, drop_database
    import os

    if app.config['BACKEND'] == "sqlite3":
        try:
            os.remove(app.config['DBPATH'])
        except:
            raise
    if database_exists(app.config['SQLALCHEMY_DATABASE_URI']):
        drop_database(app.config['SQLALCHEMY_DATABASE_URI'])

    print("Database dropped.")


def createdb():
    """Create database and tables."""
    from app import app
    from sqlalchemy_utils import database_exists, create_database
    import os
    # ensure database directory
    if app.config['BACKEND'] == 'sqlite3':
        if not os.path.exists(app.config['DBDIR']):
            os.mkdir(app.config['DBDIR'])
    if not database_exists(app.config['SQLALCHEMY_DATABASE_URI']):
        create_database(app.config['SQLALCHEMY_DATABASE_URI'])

    db.create_all(app=app)
    print("DB created.")


def resetdb():
    dropdb()
    createdb()
    print("DB Resetted")


def load_fixtures():
    import configparser
    import random
    from faker import Faker

    fake = Faker()
    config = configparser.ConfigParser()
    config.read('config.ini')

    generator_data_amount = int(config['GENERATOR']['DATA_AMOUNT'])

    def generate_sectors():
        from models import Sector

        data = []
        cnt = 0

        for _ in range(generator_data_amount):
            data.append(
                Sector(id=cnt, name=fake.catch_phrase()))
            cnt = cnt + 1

        return data

    def generate_persons():
        from models import Person

        data = []
        cnt = 0

        for _ in range(generator_data_amount):
            data.append(
                Person(id=cnt, first_name=fake.first_name(), last_name=fake.last_name(), gender=random.randint(0, 1),
                       location_id=(cnt + random.randint(1, 5)) % (generator_data_amount / 2)))
            cnt = cnt + 1

        return data

    def generate_location():
        from models import Location

        data = []
        cnt = 0

        for _ in range(generator_data_amount):
            data.append(
                Location(id=cnt, name=fake.slug(), street=fake.street_address(), city=fake.city(),
                         zip=fake.postalcode(), country=fake.country()))
            cnt = cnt + 1

        return data

    def generate_companies():
        from models import Company

        data = []
        cnt = 0

        for _ in range(generator_data_amount):
            data.append(
                Company(id=cnt, name=fake.company(), sector_id=random.randint(0, (generator_data_amount // 4)),
                        location_id=(cnt + random.randint(1, 5)) % (generator_data_amount / 2)))
            cnt = cnt + 1

        return data

    def add_fixtures():
        db.session.add_all(generate_sectors())
        print("1 of 4")
        db.session.add_all(generate_persons())
        print("2 of 4")
        db.session.add_all(generate_location())
        print("3 of 4")
        db.session.add_all(generate_companies())
        print("4 of 4")

    add_fixtures()
    db.session.commit()
    print("Fixtures loaded.")
