# Thesis-App
Welcome to my application for my thesis at Hochschule Darmstadt.
## Features
* Generating a specific amount of testdata
    * pregenerated database with 100,1.000,10.000,100.000,250.000 data     included
* Simultaneous endpoints for REST and GraphQL activated in the same application
    * All REST and GraphQL Endpints are able to be queried with full set of methods
## Installation & Running
### Requirements
* [Conda](https://github.com/conda/conda)
### Getting Started
    conda env create -f environment.yml
    flask run --host=0.0.0.0 --port=5000
